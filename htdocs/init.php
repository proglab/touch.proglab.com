<?php
require_once __DIR__.'/../vendor/autoload.php';

set_error_handler([new \Touch\Handler\Error, 'error']);

/**
 * Variables d'environnement
 */

$dotenv = new Dotenv\Dotenv(__DIR__.'/../');
$dotenv->overload();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

/**
 * Monolog
 */
$log = new Monolog\Logger('app');
$log->pushHandler(new Monolog\Handler\StreamHandler('../var/logs/app.log', Monolog\Logger::INFO));
$log->info('Initialisation de l\'application');

/**
 * Twig
 */
$loader = new Twig_Loader_Filesystem('../templates');
$twig = new Twig_Environment($loader, array(
    'cache' => '/../var/cache',
    'auto_reload' => true
));
//add truncate and wordwrap extension on twig
$twig->addExtension(new Twig_Extensions_Extension_Text());

/**
 * Base de données
 */

use Touch\Db\PdoTouch;


$resultat = PdoTouch::getInstance();
