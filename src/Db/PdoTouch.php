<?php
namespace Touch\Db;

use \PDO;

/**
 * Gestion de la base de données
 *
 * $DB->query("SELECT * FROM fruit WHERE name=:name and color=:color",array('name'=>'apple','color'=>'red'));
 * $DB->query("SELECT * FROM fruit WHERE name IN (:fruits)",array(array('apple','banana')));
 * $DB->column("SELECT color FROM fruit WHERE name IN (:color)",array('apple','banana','watermelon'));
 * $DB->row("SELECT * FROM fruit WHERE name=? and color=?",array('apple','red'));
 * $DB->single("SELECT color FROM fruit WHERE name=? ",array('watermelon'));
 * $DB->query("DELETE FROM fruit WHERE id = :id", array("id"=>"1"));
 * $DB->lastInsertId();
 *
 * @package Touch\Db
 */
class PdoTouch
{
	/**
	 * @var string $Host Host de la DB
	 * @var string $DBName nom de la DB
	 * @var string $DBUser user de la DB
	 * @var string $DBPassword password de la DB
	 * @var integer $DBPort port de la DB
	 * @var PdoTouch $_instance Instance de la DB
	 * @var \PDOStatement|bool $sQuery
	 * @var array $parameters paramètres de la requête
	 * @var int $rowCount nombre d'enregistrements
	 * @var int $columnCount nombre de colonne
	 * @var int $querycount nombre de query
	 *
	 */
	private $Host;
	private $DBName;
	private $DBUser;
	private $DBPassword;
	private $DBPort;
	private static $_instance = null;
	private $sQuery;
	private $parameters;
	public $rowCount   = 0;
	public $columnCount   = 0;
	public $querycount = 0;


	/**
	 * PdoTouch constructor.
	 * @throws \Exception
	 */
	private function __construct()
	{
		$this->Host       = getenv('DB_HOST');
		$this->DBName     = getenv('DB_NAME');
		$this->DBUser     = getenv('DB_USER');
		$this->DBPassword = getenv('DB_PASS');
		$this->DBPort	  = getenv('DB_PORT');
		$this->parameters = array();

		try {
			$this->pdo = new PDO('mysql:dbname=' . $this->DBName . ';host=' . $this->Host . ';port=' . $this->DBPort . ';charset=utf8',
				$this->DBUser,
				$this->DBPassword,
				[
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
					PDO::ATTR_EMULATE_PREPARES => false,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
				]
			);

		}
		catch (\Exception $e) {
			echo $this->ExceptionLog($e->getMessage());
			die();
		}
	}

	/**
	 * Get the instance of the database
	 *
	 * @return null|PdoTouch
	 *
	 * @throws \Exception
	 */
	public static function getInstance(): PdoTouch
	{
		if (is_null(self::$_instance ))
		{
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Close de connexion of the database
	 *
	 * @param
	 *
	 * @return
	 */
	public function CloseConnection(): void
	{
		$this->pdo = null;
	}


	/**
	 * Initialisation
	 *
	 * @param string $query
	 * @param string $parameters
	 *
	 * @throws \Exception
	 */
	private function Init(string $query, array $parameters = []): void
	{
		try {
			$this->parameters = $parameters;
			$this->sQuery     = $this->pdo->prepare($this->BuildParams($query, $this->parameters));

			if (!empty($this->parameters)) {
				if (array_key_exists(0, $parameters)) {
					$parametersType = true;
					array_unshift($this->parameters, "");
					unset($this->parameters[0]);
				} else {
					$parametersType = false;
				}
				foreach ($this->parameters as $column => $value) {
					$this->sQuery->bindParam($parametersType ? intval($column) : ":" . $column, $this->parameters[$column]); //It would be query after loop end(before 'sQuery->execute()').It is wrong to use $value.
				}
			}

			$this->succes = $this->sQuery->execute();
			$this->querycount++;
		}
		catch (\Exception $e) {
			echo $this->ExceptionLog($e->getMessage(), $this->BuildParams($query));
			die();
		}

		$this->parameters = array();
	}

	/**
	 * Construire les paramètres
	 *
	 * @param string $query
	 * @param array $params
	 *
	 * @return string
	 */
	private function BuildParams(string $query, array $params = array()): string
	{
		if (!empty($params)) {
			$array_parameter_found = false;
			foreach ($params as $parameter_key => $parameter) {
				if (is_array($parameter)){
					$array_parameter_found = true;
					$in = "";
					foreach ($parameter as $key => $value){
						$name_placeholder = $parameter_key."_".$key;
						// concatenates params as named placeholders
						$in .= ":".$name_placeholder.", ";
						// adds each single parameter to $params
						$params[$name_placeholder] = $value;
					}
					$in = rtrim($in, ", ");
					$query = preg_replace("/:".$parameter_key."/", $in, $query);
					// removes array form $params
					unset($params[$parameter_key]);
				}
			}
			// updates $this->params if $params and $query have changed
			if ($array_parameter_found) $this->parameters = $params;
		}
		return $query;
	}

	/**
	 * Do a query to the database
	 *
	 * @param string $query
	 * @param string|null $params
	 * @param int $fetchmode
	 *
	 * @return null|int|array
	 *
	 * @throws \Exception
	 */
	public function query(string $query, array $params = [], int $fetchmode = PDO::FETCH_ASSOC)
	{
		$query        = trim($query);
		$rawStatement = explode(" ", $query);
		$this->Init($query, $params);
		$statement = strtolower($rawStatement[0]);
		if ($statement === 'select' || $statement === 'show') {
			return $this->sQuery->fetchAll($fetchmode);
		} elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
			return $this->sQuery->rowCount();
		} else {
			return NULL;
		}
	}

	/**
	 * Récupérer le dernier ID inséré dans la base de donnée
	 *
	 * @return integer
	 */
	public function lastInsertId(): int
	{
		return $this->pdo->lastInsertId();
	}

	/**
	 * Récupérer le nom des colonnes d'un résultat
	 *
	 * @param string $query
	 * @param string|null $params
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function column(string $query, array $params = []): array
	{
		$this->Init($query, $params);
		$resultColumn = $this->sQuery->fetchAll(PDO::FETCH_COLUMN);
		$this->rowCount = $this->sQuery->rowCount();
		$this->columnCount = $this->sQuery->columnCount();
		$this->sQuery->closeCursor();
		return $resultColumn;
	}

	/**
	 * Récupérer 1 résultat
	 *
	 * @param string $query
	 * @param string|null $params
	 * @param int $fetchmode
	 *
	 * @return mixed
	 *
	 * @throws \Exception
	 */
	public function row(string $query, array $params = [], int $fetchmode = PDO::FETCH_ASSOC): array
	{
		$this->Init($query, $params);
		$resultRow = $this->sQuery->fetch($fetchmode);
		$this->rowCount = $this->sQuery->rowCount();
		$this->columnCount = $this->sQuery->columnCount();
		$this->sQuery->closeCursor();
		return $resultRow;
	}

	/**
	 * Récupérer le résultat d'une colonne
	 *
	 * @param $query
	 * @param null $params
	 *
	 * @return string
	 *
	 * @throws \Exception
	 */
	public function single($query, array $params = []): string
	{
		$this->Init($query, $params);
		return $this->sQuery->fetchColumn();
	}

	/**
	 * @param $message
	 * @param string $sql
	 *
	 * @return string
	 *
	 * @throws \Exception
	 *
	 */
	private function ExceptionLog($message, $sql = "")
	{
		$exception = 'Unhandled Exception. <br />';
		$exception .= $message;
		$exception .= "<br /> You can find the error back in the log.";

		if (!empty($sql)) {
			$message .= "\r\nRaw SQL : " . $sql;
		}

		trigger_error($exception, E_USER_ERROR);
		return $exception;
	}
}